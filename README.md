## mini-benchmarker
This is a small bash script for Arch-compatible distros that measures the time it takes to run a selection of benchmarks and stress tests listed below:

* perf sched & mem
* stress-ng cpu & mem
* xz compression
* ffmpeg compilation
* y-cruncher pi calculation
* argon2 hashing
* blender rendering
* primesieve
* kernel defconfig
* namd
* x265 encoding

---

Also available in [AUR](https://aur.archlinux.org/packages/mini-benchmarker).

If not installed through AUR, the user must manually install the required dependencies.

**Note:** Running this script can take a very long time on slow computers, especially those with a low CPU core count.  A dual-core Skylake CPU can easily take 50+ minutes for a single run.
